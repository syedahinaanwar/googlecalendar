<?php

use Illuminate\Support\Facades\Route;


Route::get('/', function () {
    return view('welcome');
})->name('home');
Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
    'login' =>false,
  ]);
Route::post('/create-event', 'HomeController@createEvent')->name('createEvent');

Route::get('/get-event', 'HomeController@getEvent')->name('getEvent');
