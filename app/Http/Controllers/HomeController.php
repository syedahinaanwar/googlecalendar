<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Spatie\GoogleCalendar\Event;

class HomeController extends Controller
{
    
    public function index()
    {
        return view('welcome');
    }

    public function createEvent(Request $request)
    {
        $startDateTime = Carbon::parse($request->datetimepicker);
        $endDateTime = $startDateTime;
        $event = Event::create([
            'name' => $request->title,
            'description' => $request->detail,
            'startDateTime' => $startDateTime,
            'endDateTime' => $startDateTime,
         ]);
         if($event){
            return back()->with('success','Event Created Successfully!');
         }
         else
         {
            return back()->with('error','Something went wrong.');
         }
    }
    public function getEvent()
    {
        $events = Event::get();

        return view('events')->with('events' , $events);
    }
}
