@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
    
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">All Event <a href="{{route('home')}}" class="float-right">Add Event</a></div>
               
                <div class="card-body">
                    <dl>
                    @foreach($events as $key => $event)
                        <dt>
                            {{$key + 1}}. &nbsp; {{$event->googleEvent->summary}}
                        </dt>
                        <dd>{{$event->googleEvent->description}}</dd>
                        <dd class="mb-4">{{date('j F Y H:i A \G\M\T O',strtotime($event->googleEvent->start->dateTime))}}</dd>
                    @endforeach
                    </dl>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection